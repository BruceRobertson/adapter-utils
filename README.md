Itential Adapter Utilities
===

Itential Adapter Utilities is a set of Node.js runtime libraries used by Itential Adapters.

# Usage

Include `@itentialopensource/adapter-utils` as a dependency and require it:

```
const { RequestHandler, PropertyUtility } = require('@itentialopensource/adapter-utils');
```
