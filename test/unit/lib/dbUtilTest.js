// Set globals
/* global pronghornProps */

// include required items for testing & logging
const assert = require('assert');
// const fs = require('fs');
const winston = require('winston');

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const stub = true;
const attemptTimeout = 10000;

// these variables can be changed to run in integrated mode so easier to set
// them here always check these in with bogus data!!!
const host = 'servicenow.dummy.instance';
const username = 'dummy';
const password = 'dummy';

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [
      {
        id: 'test1',
        properties: {
          host,
          port: 443,
          version: 'v1',
          authentication: {
            auth_method: 'basic user_password',
            username,
            password,
            token: 'token',
            token_user_field: 'username',
            token_password_field: 'password',
            token_result_field: 'token',
            token_URI_path: '/authenticate',
            invalid_token_error: 401,
            auth_field: 'header.auth',
            auth_field_format: '{username}:{password}'
          },
          throttle: {
            throttle_enabled: false,
            number_pronghorns: 1,
            sync_async: 'sync',
            max_in_queue: 1000,
            concurrent_max: 1,
            expire_timeout: 0,
            avg_runtime: 200
          },
          request: {
            number_retries: 3,
            limit_retry_error: 0,
            attempt_timeout: attemptTimeout,
            healthcheck_on_timeout: false,
            archiving: false
          },
          proxy: {
            enabled: false,
            host: '',
            port: 1
          },
          ssl: {
            enabled: false,
            accept_invalid_cert: false,
            ca_file: '',
            ciphers: ''
          },
          healthcheck: {
            type: 'startup',
            protocol: 'REST',
            frequency: 60000,
            URI_Path: '/api/now/table/sys_user_group'
          },
          mongo: {
            host: '',
            port: 27017,
            database: 'test1',
            username: '',
            password: ''
          },
          protocol: 'https',
          stub
        },
        type: 'test1'
      },
      {
        id: 'test2',
        properties: {
          host,
          port: 443,
          version: 'v1',
          authentication: {
            auth_method: 'basic user_password',
            username,
            password,
            token: 'token',
            token_user_field: 'username',
            token_password_field: 'password',
            token_result_field: 'token',
            token_URI_path: '/authenticate',
            invalid_token_error: 401,
            auth_field: 'header.auth',
            auth_field_format: '{username}:{password}'
          },
          throttle: {
            throttle_enabled: false,
            number_pronghorns: 1,
            sync_async: 'sync',
            max_in_queue: 1000,
            concurrent_max: 1,
            expire_timeout: 0,
            avg_runtime: 200
          },
          request: {
            number_retries: 3,
            limit_retry_error: 0,
            attempt_timeout: attemptTimeout,
            healthcheck_on_timeout: false,
            archiving: false
          },
          proxy: {
            enabled: false,
            host: '',
            port: 1
          },
          ssl: {
            enabled: false,
            accept_invalid_cert: false,
            ca_file: '',
            ciphers: ''
          },
          healthcheck: {
            type: 'startup',
            protocol: 'REST',
            frequency: 60000,
            URI_Path: '/api/now/table/sys_user_group'
          },
          mongo: {
            host: '',
            port: 0,
            database: '',
            username: '',
            password: ''
          },
          protocol: 'https',
          stub
        },
        type: 'test2'
      }
    ]
  }
};

const dbinfo = null;
// DATABASE TESTING
// also set test1 mongo.host to localhost
// const dbinfo = {
//   dburl: 'mongodb://localhost:27017',
//   database: 'test3'
// };

global.$HOME = `${__dirname}/../..`;

// set the log levels that IAP uses, spam and trace are not defaulted
// in so without this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

const DBUtil = require('../../../lib/dbUtil');

describe('[unit] Adapter Library Test', () => {
  describe('DbUtil Class Tests', () => {
    const entityName = 'template_entity';
    const actionName = 'getEntities';
    const dbInst1 = new DBUtil(pronghornProps.adapterProps.adapters[0].id, pronghornProps.adapterProps.adapters[0].properties, '..');
    const dbInst2 = new DBUtil(pronghornProps.adapterProps.adapters[1].id, pronghornProps.adapterProps.adapters[1].properties, '..');

    describe('#connect', () => {
      it('test connecting (valid adapter database)', (done) => {
        try {
          dbInst1.connect((alive, db) => {
            if (dbInst1.props.mongo.host) {
              assert.equal(alive, true);
              assert.notEqual(db, null);
              done();
            } else {
              assert.equal(alive, false);
              assert.equal(db, null);
              done();
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('test connecting (no adapter database)', (done) => {
        try {
          dbInst2.connect((alive, db) => {
            assert.equal(alive, false);
            assert.equal(db, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCollection', () => {
      it('filesystem - error on create collection - no collection name', (done) => {
        try {
          dbInst2.createCollection(null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-createCollection: Missing Collection Name or not string', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on create collection - no collection name', (done) => {
        try {
          dbInst1.createCollection(null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-createCollection: Missing Collection Name or not string', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on create collection - no collection name', (done) => {
        try {
          dbInst1.createCollection(null, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-createCollection: Missing Collection Name or not string', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - repeat create collection: no error, returns message', (done) => {
        try {
          dbInst2.createCollection('newCollection', null, true, (err, res) => {
            assert.equal(err, null);
            assert.equal('newCollection', res);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - repeat create collection: no error, returns message', (done) => {
        try {
          dbInst1.createCollection('newCollection', null, true, (err, res) => {
            assert.equal(err, null);
            assert.equal('newCollection', res);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - repeat create collection: no error, returns message', (done) => {
        try {
          dbInst1.createCollection('newCollection', dbinfo, true, (err, res) => {
            assert.equal(err, null);
            assert.equal('newCollection', res);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - should create a new collection', (done) => {
        try {
          dbInst2.createCollection('coll', null, true, (err, res) => {
            assert.equal(err, null);
            assert.equal('coll', res);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - should create a new collection', (done) => {
        try {
          dbInst1.createCollection('coll', null, true, (err, res) => {
            assert.equal(err, null);
            assert.equal('coll', res);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - should create a new collection', (done) => {
        try {
          dbInst1.createCollection('coll', dbinfo, true, (err, res) => {
            assert.equal(err, null);
            assert.equal('coll', res);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#create', () => {
      it('filesystem - error on create - no collection name', (done) => {
        try {
          dbInst2.create(null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-create: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on create - no collection name', (done) => {
        try {
          dbInst1.create(null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-create: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on create - no collection name', (done) => {
        try {
          dbInst1.create(null, null, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-create: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - error on create - no data', (done) => {
        try {
          dbInst2.create('coll', null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-create: Missing data to add', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on create - no data', (done) => {
        try {
          dbInst1.create('coll', null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-create: Missing data to add', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on create - no data', (done) => {
        try {
          dbInst1.create('coll', null, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-create: Missing data to add', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - should return data back to user', (done) => {
        try {
          const data = {
            entity: entityName,
            action: actionName
          };
          // Currently, dbUtil create and findAndModify methods require entities & actions. This may be changed later.
          dbInst2.create('coll', data, null, true, (err, res) => {
            assert.equal(err, null);
            assert.equal(res, data);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - should return data back to user', (done) => {
        try {
          const data = {
            entity: entityName,
            action: actionName
          };
          // Currently, dbUtil create and findAndModify methods require entities & actions. This may be changed later.
          dbInst1.create('coll', data, null, true, (err, res) => {
            assert.equal(err, null);
            assert.equal(res, data);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - should return data back to user', (done) => {
        try {
          const data = {
            entity: entityName,
            action: actionName
          };
          // Currently, dbUtil create and findAndModify methods require entities & actions. This may be changed later.
          dbInst1.create('coll', data, dbinfo, true, (err, res) => {
            assert.equal(err, null);
            assert.equal(res, data);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#find', () => {
      it('filesystem - error on find - no collection name', (done) => {
        try {
          dbInst2.find(null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-find: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on find - no collection name', (done) => {
        try {
          dbInst1.find(null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-find: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on find - no collection name', (done) => {
        try {
          dbInst1.find(null, null, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-find: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - should work without entity + action.', (done) => {
        try {
          dbInst2.find('coll', null, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - should work without entity + action.', (done) => {
        try {
          dbInst1.find('coll', null, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - should work without entity + action.', (done) => {
        try {
          dbInst1.find('coll', null, dbinfo, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - should return all documents found.', (done) => {
        try {
          dbInst2.find('coll', {}, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - should return all documents found.', (done) => {
        try {
          dbInst1.find('coll', {}, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - should return all documents found.', (done) => {
        try {
          dbInst1.find('coll', {}, dbinfo, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIndex', () => {
      it('filesystem - error on create index - no collection name', (done) => {
        try {
          dbInst2.createIndex(null, null, null, null, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-createIndex: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on create index - no collection name', (done) => {
        try {
          dbInst1.createIndex(null, null, null, null, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-createIndex: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on create index - no collection name', (done) => {
        try {
          dbInst1.createIndex(null, null, dbinfo, null, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-createIndex: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - error on create index - no spec document', (done) => {
        try {
          dbInst2.createIndex('coll', null, null, null, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-createIndex: Missing Specs', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on create index - no spec document', (done) => {
        try {
          dbInst1.createIndex('coll', null, null, null, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-createIndex: Missing Specs', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on create index - no spec document', (done) => {
        try {
          dbInst1.createIndex('coll', null, dbinfo, null, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-createIndex: Missing Specs', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - error on create index - no database', (done) => {
        try {
          dbInst2.createIndex('coll', { transNum: 1 }, null, null, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-createIndex: No database - no index', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - create index will work with database connection.', (done) => {
        try {
          dbInst1.createIndex('coll', { transNum: 1 }, null, null, (err, res) => {
            if (dbInst1.props.mongo.host) {
              assert.equal(err, null);
              assert.notEqual(res, null);
              done();
            } else {
              assert.notEqual(err, null);
              assert.equal(res, null);
              assert.equal('test1-dbUtil-createIndex: No database - no index', err);
              done();
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - create index will work with database connection.', (done) => {
        try {
          dbInst1.createIndex('coll', { transNum: 1 }, null, dbinfo, (err, res) => {
            if (dbinfo) {
              assert.equal(err, null);
              assert.notEqual(res, null);
              done();
            } else {
              assert.notEqual(err, null);
              assert.equal(res, null);
              assert.equal('test1-dbUtil-createIndex: No database - no index', err);
              done();
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#countDocuments', () => {
      it('filesystem - error on count Documents - no collection name', (done) => {
        try {
          dbInst2.countDocuments(null, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-countDocuments: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on count Documents - no collection name', (done) => {
        try {
          dbInst1.countDocuments(null, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-countDocuments: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on count Documents - no collection name', (done) => {
        try {
          dbInst1.countDocuments(null, null, null, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-countDocuments: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - count documents should return -1 if no db conn, else #. Query empty: that is mongo issue if err', (done) => {
        try {
          dbInst2.countDocuments('coll', {}, {}, null, true, (err, res) => {
            assert.equal(err, null);
            assert.equal(typeof res, 'number');
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - count documents should return -1 if no db conn, else #. Query empty: that is mongo issue if err', (done) => {
        try {
          dbInst1.countDocuments('coll', {}, {}, null, true, (err, res) => {
            assert.equal(err, null);
            assert.equal(typeof res, 'number');
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - count documents should return -1 if no db conn, else #. Query empty: that is mongo issue if err', (done) => {
        try {
          dbInst1.countDocuments('coll', {}, {}, dbinfo, true, (err, res) => {
            assert.equal(err, null);
            assert.equal(typeof res, 'number');
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete', () => {
      it('filesystem - error on delete - no collection name', (done) => {
        try {
          dbInst2.delete(null, null, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-delete: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on delete - no collection name', (done) => {
        try {
          dbInst1.delete(null, null, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-delete: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on delete - no collection name', (done) => {
        try {
          dbInst1.delete(null, null, null, null, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-delete: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - error on delete - no filter', (done) => {
        try {
          dbInst2.delete('coll', null, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-delete: Missing Filter', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on delete - no filter', (done) => {
        try {
          dbInst1.delete('coll', null, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-delete: Missing Filter', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on delete - no filter', (done) => {
        try {
          dbInst1.delete('coll', null, null, null, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-delete: Missing Filter', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - error on delete - no multiple boolean', (done) => {
        try {
          dbInst2.delete('coll', {}, {}, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-delete: Missing Multiple flag', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on delete - no multiple boolean', (done) => {
        try {
          dbInst1.delete('coll', {}, {}, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-delete: Missing Multiple flag', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on delete - no multiple boolean', (done) => {
        try {
          dbInst1.delete('coll', {}, {}, null, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-delete: Missing Multiple flag', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - should delete 1 possible match', (done) => {
        try {
          dbInst2.delete('coll', {}, {}, false, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - should delete 1 possible match', (done) => {
        try {
          dbInst1.delete('coll', {}, {}, false, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - should delete 1 possible match', (done) => {
        try {
          dbInst1.delete('coll', {}, {}, false, dbinfo, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - should delete all possible matches', (done) => {
        try {
          dbInst2.delete('coll', {}, {}, true, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - should delete all possible matches', (done) => {
        try {
          dbInst1.delete('coll', {}, {}, true, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - should delete all possible matches', (done) => {
        try {
          dbInst1.delete('coll', {}, {}, true, dbinfo, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceOne', () => {
      it('filesystem - error on replace one - no collection name', (done) => {
        try {
          dbInst2.replaceOne(null, null, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-replaceOne: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on replace one - no collection name', (done) => {
        try {
          dbInst1.replaceOne(null, null, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-replaceOne: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on replace one - no collection name', (done) => {
        try {
          dbInst1.replaceOne(null, null, null, null, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-replaceOne: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - error on replace one - no filter', (done) => {
        try {
          dbInst2.replaceOne('coll', null, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-replaceOne: Missing Filter', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on replace one - no filter', (done) => {
        try {
          dbInst1.replaceOne('coll', null, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-replaceOne: Missing Filter', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on replace one - no filter', (done) => {
        try {
          dbInst1.replaceOne('coll', null, null, null, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-replaceOne: Missing Filter', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - error on replace one - no doc', (done) => {
        try {
          dbInst2.replaceOne('coll', {}, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-replaceOne: Missing Document', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on replace one - no doc', (done) => {
        try {
          dbInst1.replaceOne('coll', {}, null, null, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-replaceOne: Missing Document', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on replace one - no doc', (done) => {
        try {
          dbInst1.replaceOne('coll', {}, null, null, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-replaceOne: Missing Document', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - should replace one in the database--if no connection, returns string.', (done) => {
        try {
          const data = {
            entity: 'someOtherEntity',
            action: 'someOtherAction',
            num_called: 2
          };
          dbInst2.replaceOne('coll', {}, data, {}, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - should replace one in the database--if no connection, returns string.', (done) => {
        try {
          const data = {
            entity: 'someOtherEntity',
            action: 'someOtherAction',
            num_called: 2
          };
          dbInst1.replaceOne('coll', {}, data, {}, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - should replace one in the database--if no connection, returns string.', (done) => {
        try {
          const data = {
            entity: 'someOtherEntity',
            action: 'someOtherAction',
            num_called: 2
          };
          dbInst1.replaceOne('coll', {}, data, {}, dbinfo, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeCollection', () => {
      it('filesystem - error on remove collection - no collection name', (done) => {
        try {
          dbInst2.removeCollection(null, null, true, (err, res) => {
            if (err) {
              assert.notEqual(err, null);
              assert.equal(res, null);
            }
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on remove collection - no collection name', (done) => {
        try {
          dbInst1.removeCollection(null, null, true, (err, res) => {
            if (err) {
              assert.notEqual(err, null);
              assert.equal(res, null);
            }
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on remove collection - no collection name', (done) => {
        try {
          dbInst1.removeCollection(null, dbinfo, true, (err, res) => {
            if (err) {
              assert.notEqual(err, null);
              assert.equal(res, null);
            }
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - should remove collection if exists - otherwise returns string', (done) => {
        try {
          dbInst2.removeCollection('newCollection', null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - should remove collection if exists - otherwise returns string', (done) => {
        try {
          dbInst1.removeCollection('newCollection', null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - should remove collection if exists - otherwise returns string', (done) => {
        try {
          dbInst1.removeCollection('newCollection', dbinfo, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findAndModify', () => {
      it('filesystem - error on find and modify - no collection name', (done) => {
        try {
          const data = {
            $inc: {},
            $set: {}
          };
          dbInst2.findAndModify(null, null, null, data, true, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-findAndModify: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on find and modify - no collection name', (done) => {
        try {
          const data = {
            $inc: {},
            $set: {}
          };
          dbInst1.findAndModify(null, null, null, data, true, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-findAndModify: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on find and modify - no collection name', (done) => {
        try {
          const data = {
            $inc: {},
            $set: {}
          };
          dbInst1.findAndModify(null, null, null, data, true, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-findAndModify: Missing Collection Name', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - error on find and modify - no data', (done) => {
        try {
          dbInst2.findAndModify('coll', null, null, null, true, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test2-dbUtil-findAndModify: Missing data for modification', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - error on find and modify - no data', (done) => {
        try {
          dbInst1.findAndModify('coll', null, null, null, true, null, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-findAndModify: Missing data for modification', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - error on find and modify - no data', (done) => {
        try {
          dbInst1.findAndModify('coll', null, null, null, true, dbinfo, true, (err, res) => {
            assert.notEqual(err, null);
            assert.equal(res, null);
            assert.equal('test1-dbUtil-findAndModify: Missing data for modification', err);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('filesystem - should return findOneAndUpdate result unless offline, then returns JSON data pushed', (done) => {
        try {
          const data = {
            $inc: { num_called: 1 },
            $set: {
              entity: 'someEntity',
              action: 'someAction'
            }
          };
          dbInst2.findAndModify('coll', null, null, data, true, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('adapter db - should return findOneAndUpdate result unless offline, then returns JSON data pushed', (done) => {
        try {
          const data = {
            $inc: { num_called: 1 },
            $set: {
              entity: 'someEntity',
              action: 'someAction'
            }
          };
          dbInst1.findAndModify('coll', null, null, data, true, null, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('dbinfo - should return findOneAndUpdate result unless offline, then returns JSON data pushed', (done) => {
        try {
          const data = {
            $inc: { num_called: 1 },
            $set: {
              entity: 'someEntity',
              action: 'someAction'
            }
          };
          dbInst1.findAndModify('coll', null, null, data, true, dbinfo, true, (err, res) => {
            assert.equal(err, null);
            assert.notEqual(res, null);
            done();
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disconnect', () => {
      it('should disconnect', (done) => {
        try {
          if (dbInst1) {
            dbInst1.disconnect();
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
