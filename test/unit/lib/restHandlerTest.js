// Set globals
/* eslint no-loop-func:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const winston = require('winston');

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'debug';
const stub = true;
const attemptTimeout = 10000;

// these variables can be changed to run in integrated mode so easier to set
// them here always check these in with bogus data!!!
const host = 'servicenow.dummy.instance';
const username = 'dummy';
const password = 'dummy';

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
const pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'test',
      properties: {
        host,
        port: 443,
        version: 'v1',
        authentication: {
          auth_method: 'basic user_password',
          username,
          password,
          token: 'token',
          token_user_field: 'username',
          token_password_field: 'password',
          token_result_field: 'token',
          token_URI_path: '/authenticate',
          invalid_token_error: 401,
          auth_field: 'header.auth',
          auth_field_format: '{username}:{password}'
        },
        throttle: {
          throttle_enabled: false,
          number_pronghorns: 1,
          sync_async: 'sync',
          max_in_queue: 1000,
          concurrent_max: 1,
          expire_timeout: 0,
          avg_runtime: 200
        },
        request: {
          number_retries: 3,
          limit_retry_error: 0,
          attempt_timeout: attemptTimeout,
          healthcheck_on_timeout: false,
          archiving: false
        },
        proxy: {
          enabled: false,
          host: '',
          port: 1
        },
        ssl: {
          enabled: false,
          accept_invalid_cert: false,
          ca_file: '',
          ciphers: ''
        },
        healthcheck: {
          type: 'startup',
          protocol: 'REST',
          frequency: 60000,
          URI_Path: '/api/now/table/sys_user_group'
        },
        protocol: 'https',
        stub
      },
      type: 'test'
    }]
  }
};
const errorProps = JSON.parse(JSON.stringify(pronghornProps));
let finishedTests = 0;
let running = false;

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted
// in so without this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

// require the request handler as that is the entry into the Adapter Libraries
const PropUtil = require('../../../lib/propertyUtil');
const TransUtil = require('../../../lib/translatorUtil');
const Connector = require('../../../lib/connectorRest');
const RestHandler = require('../../../lib/restHandler');

// begin the testing - these should be pretty well defined between the describe
// and the it!
describe('[unit] Adapter Library Test', () => {
  describe('Rest Handler Class Tests', () => {
    const propInst = new PropUtil(pronghornProps.adapterProps.adapters[0].id, 'test');
    const transInst = new TransUtil(pronghornProps.adapterProps.adapters[0].id, propInst);
    const connectInst = new Connector(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties,
      transInst,
      propInst
    );

    const a = new RestHandler(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties,
      connectInst,
      transInst
    );

    // Read the internal entity schema from the file system
    const schemaGet = JSON.parse(fs.readFileSync('test/utilsinternalget.json', 'utf-8'));
    const schemaPost = JSON.parse(fs.readFileSync('test/utilsinternalpost.json', 'utf-8'));
    const schemaPut = JSON.parse(fs.readFileSync('test/utilsinternalput.json', 'utf-8'));
    const schemaBad1 = JSON.parse(fs.readFileSync('test/utilsinternalbad1.json', 'utf-8'));
    const schemaBad2 = JSON.parse(fs.readFileSync('test/utilsinternalbad2.json', 'utf-8'));
    const schemaGetNoMock = JSON.parse(fs.readFileSync('test/utilsinternalgetnomock.json', 'utf-8'));

    const entityName = 'template_entity';
    describe('#genericRestRequest - errors', () => {
      it('error on generic rest request - no entity', (done) => {
        const p = new Promise((resolve) => {
          a.genericRestRequest(null, null, null, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.300', error.icode);
            assert.equal('test-restHandler-genericRestRequest', error.IAPerror.origin);
            assert.equal('Entity is required', error.IAPerror.displayString);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on generic rest request - no action', (done) => {
        const p = new Promise((resolve) => {
          a.genericRestRequest(entityName, null, null, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.300', error.icode);
            assert.equal('test-restHandler-genericRestRequest', error.IAPerror.origin);
            assert.equal('Action is required', error.IAPerror.displayString);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on generic rest request - no schema', (done) => {
        const p = new Promise((resolve) => {
          a.genericRestRequest(entityName, 'getEntities', null, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.300', error.icode);
            assert.equal('test-restHandler-genericRestRequest', error.IAPerror.origin);
            assert.equal('Entity Schema is required', error.IAPerror.displayString);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on generic rest request - action missing method', (done) => {
        const p = new Promise((resolve) => {
          a.genericRestRequest(entityName, 'getEntities', schemaBad1, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.302', error.icode);
            assert.equal('test-restHandler-genericRestRequest', error.IAPerror.origin);
            assert.equal('Invalid action file: missing method in template_entity/getEntities', error.IAPerror.displayString);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on generic rest request - action missing path', (done) => {
        const p = new Promise((resolve) => {
          a.genericRestRequest(entityName, 'getEntities', schemaBad2, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.302', error.icode);
            assert.equal('test-restHandler-genericRestRequest', error.IAPerror.origin);
            assert.equal('Invalid action file: missing entity path in template_entity/getEntities', error.IAPerror.displayString);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      if (stub) {
        it('generic rest request - no mock data error', (done) => {
          const p = new Promise((resolve) => {
            const dataObj = {
              uriQuery: {
                number: 'CHG0000001'
              }
            };
            a.genericRestRequest(entityName, 'getEntities', schemaGetNoMock, dataObj, true, (data, error) => {
              resolve(data);
              assert.equal(null, data);
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.notEqual(undefined, error.IAPerror);
              assert.notEqual(null, error.IAPerror);
              assert.notEqual(undefined, error.IAPerror.displayString);
              assert.notEqual(null, error.IAPerror.displayString);
              assert.equal('AD.500', error.icode);
              assert.equal('test-connectorRest-handleEndResponse', error.IAPerror.origin);
              assert.equal('Error 400 received on request', error.IAPerror.displayString);
              finishedTests += 1;
              done();
            });
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        }).timeout(attemptTimeout);
      }
      it('generic rest request - error on query translate', (done) => {
        const p = new Promise((resolve) => {
          const dataObj = {
            uriQuery: {
              number: 543
            }
          };
          a.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.312', error.icode);
            assert.equal('test-translatorUtil-buildJSONEntity', error.IAPerror.origin);
            assert.equal('Schema validation failed on should be string', error.IAPerror.displayString);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on post rest request - missing required field no body', (done) => {
        const p = new Promise((resolve) => {
          a.genericRestRequest(entityName, 'createEntity', schemaPost, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.312', error.icode);
            assert.equal('test-translatorUtil-buildJSONEntity', error.IAPerror.origin);
            assert.equal('Schema validation failed on should have required property \'.summary\'', error.IAPerror.displayString);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on post rest request - missing required field', (done) => {
        const p = new Promise((resolve) => {
          const newObj = {
            payload: {
              name: 'blahblahblah'
            }
          };
          a.genericRestRequest(entityName, 'createEntity', schemaPost, newObj, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.312', error.icode);
            assert.equal('test-translatorUtil-buildJSONEntity', error.IAPerror.origin);
            assert.equal('Schema validation failed on should have required property \'.summary\'', error.IAPerror.displayString);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('error on post rest request - missing changes no body', (done) => {
        const p = new Promise((resolve) => {
          a.genericRestRequest(entityName, 'updateEntity', schemaPut, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.312', error.icode);
            assert.equal('test-translatorUtil-buildJSONEntity', error.IAPerror.origin);
            assert.equal('Schema validation failed on should have required property \'.id\'', error.IAPerror.displayString);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('#healthCheck - Bad SSL', () => {
      it('should not be healthy', (done) => {
        while (true) {
          if (finishedTests >= 9 && !running) {
            running = true;
            errorProps.adapterProps.adapters[0].properties.ssl.enabled = true;
            errorProps.adapterProps.adapters[0].properties.ssl.ca_file = 'not_there';
            errorProps.adapterProps.adapters[0].properties.ssl.ciphers = 'not_there';

            const connectInstB = new Connector(
              errorProps.adapterProps.adapters[0].id,
              errorProps.adapterProps.adapters[0].properties,
              transInst,
              propInst
            );

            const b = new RestHandler(
              errorProps.adapterProps.adapters[0].id,
              errorProps.adapterProps.adapters[0].properties,
              connectInstB,
              transInst
            );

            const p = new Promise((resolve) => {
              b.healthcheckRest(null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.301', error.icode);
                assert.equal('test-connectorRest-healthCheck', error.IAPerror.origin);
                assert.equal('Can not open file not_there', error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
            });
            // log just done to get rid of const lint issue!
            log.debug(p);
            break;
          }
        }
      }).timeout(attemptTimeout);
    });
  });
});
